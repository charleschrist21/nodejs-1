
const express =require("express");
const app = express();
const bodyParser =require('body-parser');

const MongoClient = require('mongodb').MongoClient;
const objectID = require('mongodb').ObjectID;
const DBurl = "mongodb://localhost:27017/";
const DBName = "arkademy";

let dbo = null;
MongoClient.connect(DBurl,(error, db)=>{
    if(error) throw error;
    dbo = db.db(DBName);
})

app.use(bodyParser.urlencoded({ extended: false }))

app.get('/siswa',(req,response)=>{
    dbo.collection("siswa").find().toArray((err,res)=>{
        if(err) throw err;
        response.json(res);
    })
})

app.get('/siswa/:id',(request,response)=>{
    let id = request.params.id;
    let id_object = new ObjectID(id);
    
    dbo.collection("siswa").findOne({"_id" : id_object},(error,result)=>{
        if(error) throw error;
        response.json(result);
    })
})
app.post('/siswa',(request,response)=>{
    let namaSiswa = request.body.nama;
    let alamat = request.body.alamat;

    response.end("menampilkan data dengan nama: " + namaSiswa +"dan alamat" +alamat);
})

app.delete('/siswa/:nama', (request, response)=>{
    let namaSiswa = request.params.nama;

    response.end("nama siswa: " + namaSiswa + " akan dihapus dari sistem");
})

app.put('/siswa/:id', (request,response)=>{
    let id = request.params.id;
    let namaSiswa = request.body.nama;
    let alamat = request.body.alamat;

    response.end("data dengan id: "+ id + " telah diupdate");
})


app.listen('8080', (e)=>{
    console.log(e);
})
